# Usage

```yaml
version: '3.4'

services:
  pitemp:
    image: registry.gitlab.com/yazasnyal-docker/prometheus-rpi-temp:latest
    command:
      - '--root=/host'
    ports:
      - 9101:8000
    volumes:
      - '/:/host:ro,rslave'
    deploy:
      labels:
        prometheus-job: "pitemp"
```
