import os, sys
import argparse
from prometheus_client import start_http_server, Gauge
import time

parser = argparse.ArgumentParser()
parser.add_argument('--root', metavar='rootdir', type=str, default='/',
                    help='system root directory')
parser.add_argument('--port', metavar='port', type=int, default=8000,
                    help='system root directory')
pargs = parser.parse_args()

# Get CPU's temperature
def GetCpuTemperature() -> float:
    res = os.popen('cat {}/sys/class/thermal/thermal_zone0/temp'.format(pargs.root)).readline()
    return float(res)/1000

CPU_TEMP = Gauge('prometheus_pi_cpu_temp', 'Current CPU temperature')
CPU_TEMP.set_function(GetCpuTemperature)

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(pargs.port)
    # sleep indefenetly
    while True:
        time.sleep(100)
